'use strict';

/**
 * @ngdoc function
 * @name senkouteApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the senkouteApp
 */
angular.module('senkouteApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
