'use strict';

/**
 * @ngdoc function
 * @name senkouteApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the senkouteApp
 */
angular.module('senkouteApp')
  .controller('MainCtrl', ['$rootScope', '$scope', function ($rootScope, $scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.listCompte = {
      compte1: { numero: 1, title: 'compte 1' },
      compte2: { numero: 2, title: 'compte 2' },
      compte3: { numero: 3, title: 'compte 3' },
      compte4: { numero: 4, title: 'compte 4' },
    };


    $scope.$on('mon-event', function(event, data) {
      console.log('data received', data);
      $scope.dataEnvoyees = data;
    });
    

    $scope.requestSendData = function(id) {
      $rootScope.$emit('request-send-data-' + id);
    };
  }]);