'use strict';

angular.module('senkouteApp').directive('compteBenef', ['$rootScope', function($rootScope) {
  return {
    restrict: 'E',
    scope: true,
    templateUrl: '../views/form.html',
    link: function($scope, $el, $attr) {

      $rootScope.$on('request-send-data-' + $attr['id'], function(event, data) {
        $scope.$emit('mon-event', {
          montant: $scope.montant,
          numero: ($scope.selectedCompte || {}).numero
        });
      });

      $scope.send = function() {
        console.log('data', $scope, $scope.montant, $scope.selectedCompte);

        $scope.$emit('mon-event', {
          montant: $scope.montant,
          numero: ($scope.selectedCompte || {}).numero
        });
      };
    }
  };
}]);
