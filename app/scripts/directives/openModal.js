'use strict';

angular.module('senkouteApp').directive('openModal', function() {
  return {
    restrict: 'A',
    link: function($scope, $el, $attr) {
      $el.on('click', function() {
        $('#compteModal').modal('show');
      });
    }
  };
});
