// Code goes here

angular.module('app', []);

angular.module('app').directive("exampleDirective", function(){
  return {
    templateUrl: "_directive_template.html",
    restrict: "E",
    scope: {},
    link: function(scope, el, attr) {
      scope.some_value = scope.some_value;
    }
  }
});